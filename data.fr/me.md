---
first_name: Axel
last_name: Haustant
tagline: Techlead, Développeur full stack
avatar: /images/me1.png

email: axel@apihackers.com
phone:
  display: +33 (0)7 81 14 97 07
  number: +33781149707

websites:
  -
    name: API Hackers
    link: https://apihackers.com
  -
    name: noirbizarre.info
    link: https://noirbizarre.info
  -
    name: Mon CV
    link: https://axel.haustant.fr

social:
  -
    name: github
    icon: github
    link: http://github.com/noirbizarre
    display: github.com/noirbizarre
  -
    name: Twitter
    icon: twitter
    link: http://twitter.com/noirbizarre
    display: '@noirbizarre'
  -
    name: LinkedIn
    icon: linkedin
    link: http://fr.linkedin.com/in/axelhaustant
    display: axelhaustant
  # -
  #   name: Viadeo
  #   icon: viadeo
  #   link: http://www.viadeo.com/fr/profile/axel.haustant
  #   display: axel.haustant


languages:
  -
    name: Français
    level: Langue maternelle
  -
    name: Anglais
    level: Courant
  -
    name: Espagnol
    level: Professionel


timeline:
  -
    date: 2003
    details: INSA Lyon
  -
    date: 2008
    details: Techlead @ Orange
  -
    date: 2011
    details: Techlead @ Mappy
  -
    date: 2013
    details: Teachlead @ Etalab
  -
    date: 2016
    details: CEO @ API Hackers


skills:
  -
    name: Code
    rate: 95
  -
    name: Design
    rate: 75
  -
    name: Agilité
    rate: 85
  -
    name: DevOps
    rate: 85


---

Développeur passionné, mon métier est d'apprendre votre métier pour le mettre en valeur.

Réalisation de sites, automatisation, référencement, mailing, scripting,
coaching méthodologique, agilité, devops, intégration continue, formation...
Autant de domaines dans lesquels je prends plaisir à offrir mes compétences
pour améliorer vos processus et réduire vos coûts.

L'informatique ne doit jamais être une barrière ou une contrainte dans votre métier.
Aussi je m'efforce de fournir des solutions adaptées à vos besoins.
