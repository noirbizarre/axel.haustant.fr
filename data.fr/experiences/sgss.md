---
company: Société Générale Security Services
where: La Défense
role: Consultant J2EE
from: 2011-05
to: 2011-07
---
Mission de courte durée pour effectuer les taches suivantes:

- Mise en place d'une usine logicielle J2EE (Maven / Nexus / Jenkins / Selenium)
- Couverture de l’existant en tests/scénarios Sélénium
- Mise en place de l'utilisation de MongoDB pour les métriques des OPCVM
