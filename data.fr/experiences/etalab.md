---
company: Etalab
where: Paris
from: 2013-08
to: 2019-10
role: Teachlead
---
En charge du site [Data.gouv.fr](https://www.data.gouv.fr),
la plateforme nationnale de mise à disposition des données ouvertes,
j'interviens sur l'ensemble des aspects de son cycle de vie, entre autres:

- conception et réalisation de la plateforme (Python/JavaScript)
- animation de la communauté associée (logiciel libre)
- administration de la plateforme
- administration des infrastructures data.gouv.fr et etalab.gouv.fr

Au delà des aspects et choix techniques innovant,
Data.gouv.fr c'est aussi des outils et méthodes inédits au sein de l'administration:

- l'agilité nous permet des réactifs et de livrer en un temps records
- DevOps nous permet d'opérer nous même la plateforme
- nous interagissons en direct avec la communauté

Un projet enrichissant techniquement et humainement,
le tout avec un impact social positif.
