---
company: Kompass
where: Courbevoie
from: 2010-08
to: 2011-04
role: Consultant en intégration continue
---
Mission de modernisation des outils et méthodes comprenant:

- Initialisation des méthodes agiles (Scrum / XP)
- Migration de Subversion vers Git
- Mise en place d'une plateforme d'intégration continue (Maven / Jenkins / Nexus)
- Maintenance et évolutions diverses du site web Kompass (Oracle Portal / EJB2)
