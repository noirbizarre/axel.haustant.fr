---
company: Urbandive
from: 2011-07
to: 2012-02
role: Consultant J2EE GeoData
---
Au sein l'équipe GeoData d'UrbanDive (filiale de Mappy), j’ai eu à intervenir sur les sujets suivants:

- Amélioration de l'usine logicielle (Nexus / Jenkins / Maven / shell)
- Création de batchs d'intégration des POI de partenaires (Java / MongoDB)
- Évolution, optimisation et maintenance du backoffice de floutage des vues panoramiques (Flex / php / MySQL / Python / AWS)
- Évolution de l'architecture des serveurs de POI et de vues panoramiques (Java / Jersey / REST / MongoDB / AWS)
- Industrialisation du processus de collecte et de génération des vues panoramiques intérieures (Ruby)
