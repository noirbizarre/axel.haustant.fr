---
company: Mappy
from: 2012-02
to: 2013-08
role: Techlead GeoData
where: Paris
---
Suite à la fusion de Mappy et d’UrbanDive,
j’ai intégré l’équipe Coeur de métier/Data pour aider à la migration agile
et effectuer les travaux suivants:

- Mise en place de l'intégration continue (Jenkins / shell / Python / Fabric)
- Mise en place de pratiques DevOps
- Maintenance et évolution de l'ordonnanceur de process (Python / Django)
- Création du backoffice des vues panoramiques intérieures (Python / Django / Javascript / KRPano / AWS)
- Réalisation de batchs d'intégration de données géospatiales (Python / PostgreSQL/PostGIS / Oracle Spatial / GDAL)
- Mise en place de nouvelles cartes (style, données,...):
- Intégration des données de différents fournisseurs (TomTom, IGN, ...)
- Optimisation pour les services d’itinéraire et de localisation
- Optimisation pour l’affichage à différents niveaux de zoom
- Mise en place de templates Mapnik
- Développement d'outils internes spécifiques
