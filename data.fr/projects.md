---
opensource:
  - 
    name: udata
    url: https://github.com/opendatateam/udata
    description: Un portail opendata personnalisable et extensible
  -
    name: Flask-RESTPlus
    url: https://github.com/noirbizarre/flask-restplus
    description: Des API propres et documentées pour Flask
  -
    name: Flask-FS
    url: https://github.com/noirbizarre/Flask-FS
    description: Stockage de fichiers simple et facile pour Flask
  -
    name: Gonja
    url: https://github.com/noirbizarre/gonja
    description: Une implementation de Jinja2 en Go

---

Quelques un de mes projets open-sources
