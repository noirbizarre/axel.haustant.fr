---
company: Etalab
where: Paris
from: 2013-08
to: 2019-10
role: Teachlead
---
In charge of the site [Data.gouv.fr](https://www.data.gouv.fr),
the french open data platform,
I stand on all aspects of its life cycle, including:

- Platform design and development (Python / JavaScript)
- Open-source community management (free software)
- Platform administration
- data.gouv.fr and etalab.gouv.fr infrastructures administration

Beyond the innovative aspects and selection techniques,
Data.gouv.fr is some inedit tools and methods within the administration:

- Agility gives us reactivity and delivery in record time
- DevOps enables us to maintain ourselves the platform
- We interact directly with the community

A rewarding project technically and humanly,
all with a positive social impact.
