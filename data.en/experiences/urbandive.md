---
company: Urbandive
from: 2011-07
to: 2012-02
role: J2EE and GeoData expert
---
Within the GeoData team (a Mappy subsidiary company), I had to work on the follwing topics:

- Software factory improvements (Nexus / Jenkins / Maven / shell)
- POI integration batches development for partners providers (Java / MongoDB)
- Street views blurring backoffice enhancement and optimization (Flex / php / MySQL / Python / AWS)
- POI and Street views servers architecture improvements (Java / Jersey / REST / MongoDB / AWS)
- Indoor 360 views gathering and generation process industrialization (Ruby)
