---
company: Parnasse (Orange)
where: Lyon
from: 2009-01
to: 2009-07
role: Ingénieur d’étude J2EE
---
Réalisation d’un outil de recomposition de factures au format PDF.
Projet pilote sur Scrum et sur Maven/Spring/Flex chez IT&Labs.
Réalisation complète de la conception jusqu’à la mise en exploitation.
Expérimentation d’outils et méthodes innovants chez IT&Labs :

- Intégration continue
- Test Driven Development
- Maven
- Flex / Flex mojos
