---
company: Kompass
where: Courbevoie
from: 2010-08
to: 2011-04
role: Continuous integration expert
---
Processes and tooling modernization mission, including:

- Agile methodologies bootstrap (Scrum / XP)
- Subversion to Git migration
- Continuous integration platform setup (Maven / Jenkins / Nexus)
- Miscellaneous improvements on the Kompass web site (Oracle Portal / EJB2)
