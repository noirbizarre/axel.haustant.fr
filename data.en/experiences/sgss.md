---
company: Société Générale Security Services
where: La Défense
role: J2EE Consultant
from: 2011-05
to: 2011-07
---
Short mission to perform the following tasks:

- Software factory platform setup (Maven / Jenkins / Nexus / Selenium)
- Legacy scenarios Sélénium coverage
- MongoDB usage introduction for the OPCVM metrics
