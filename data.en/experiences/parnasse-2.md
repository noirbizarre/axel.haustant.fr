---
company: Parnasse (Orange)
where: Paris
from: 2009-07
to: 2010-07
role: Techlead J2EE
---
Custom CRM implementation for Parnasse using Scrum methodology:

- Quality and technical processes management and improvements
- Software factory implementation (Nexus / Jenkins / Maven / Sonar)
- Coding guidelines realisation


- Gestion et amélioration des processus technique et qualité
- Mise en place d’une usine logicielle (Maven / Hudson / Sonar / Nexus)
- Mise en place de règles et conventions de développement
- Formalisation et automatisation du processus de livraison
- Amélioration du processus de gestion de configuration
- Conception et migration vers une nouvelle architecture :
- Découpage propre en couches
- Migration de Dojo Toolkit vers Flex 4 et Swiz
- Nettoyage de l’API REST/JSON (Spring MVC / Spring JSON)
- Exposition de l’API en AMF (BlazeDS / Spring AMF)
- Encadrement technique de l’équipe
