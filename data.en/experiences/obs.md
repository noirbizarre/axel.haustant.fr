---
company: Orange Business Services/IT&Labs
where: Lyon
from: 2008-07
to: 2009-01
role: Ingénieur d’étude
---
Amélioration de la qualité et de l’expérience utilisateur sur Woodi,
l’outil gestion de demande de plateformes de développement de France Télécom

- Dynamisation de l’interface avec jQuery
- Structuration des couches
- Mutualisation des classes utilitaires en un framework documenté
- Amélioration du processus de gestion de configuration et de livraison
