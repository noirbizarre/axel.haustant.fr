---
company: Mappy
from: 2012-02
to: 2013-08
role: Techlead GeoData
where: Paris
---
After the merge between Mappy and UrbandDive,
I join the COre/Data team to help to the agility transition
and perform the following missions:

- Continuous integration setup (Jenkins / shell / Python / Fabric)
- DevOps practices  initialization
- Process scheduler enhancement (Python / Django)
- Indoor panoramic views backoffice development (Python / Django / Javascript / KRPano / AWS)
- Spatial data integration batches development (Python / PostgreSQL/PostGIS / Oracle Spatial / GDAL)
- New maps realization (style, data,...):
- Data integration from multiple providers (TomTom, IGN, ...)
- Data optimization for the loclization and routes services
- Data optimisation for multiple zoom levels display
- Mapnik templates realization
- Specific internal tools development
