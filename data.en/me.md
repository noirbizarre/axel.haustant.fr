---
first_name: Axel
last_name: Haustant
tagline: Techlead, Full stack developer
avatar: /images/me1.png

email: axel@apihackers.com
phone:
  display: +33 (0)7 81 14 97 07
  number: +33781149707

websites:
  -
    name: API Hackers
    link: https://apihackers.com
  -
    name: noirbizarre.info
    link: https://noirbizarre.info
  -
    name: My Resume
    link: https://axel.haustant.fr

social:
  -
    name: github
    icon: github
    link: http://github.com/noirbizarre
    display: github.com/noirbizarre
  -
    name: Twitter
    icon: twitter
    link: http://twitter.com/noirbizarre
    display: '@noirbizarre'
  -
    name: LinkedIn
    icon: linkedin
    link: http://fr.linkedin.com/in/axelhaustant
    display: axelhaustant
  # -
  #   name: Viadeo
  #   icon: viadeo
  #   link: http://www.viadeo.com/fr/profile/axel.haustant
  #   display: axel.haustant


languages:
  -
    name: French
    level: Mother tongue
  -
    name: English
    level: Fluent
  -
    name: Spanish
    level: Professional


---

Passionate developer, my job is to learn yours to showcase it.

Making websites, automation, optimization, mailing, scripting,
methodological coaching, agility, DevOps, continuous integration, training ...
All areas in which I delight to offer my skills
to improve your processes and reduce costs.

Computers should never be a barrier or constraint in your job.
I try to provide solutions tailored to your needs.
