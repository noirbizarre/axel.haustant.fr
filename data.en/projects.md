---
opensource:
  - 
    name: udata (and its plugins)
    url: https://github.com/opendatateam/udata
    description: A customisable opendata portal
  -
    name: Flask-RESTPlus
    url: https://github.com/noirbizarre/flask-restplus
    description: Fully featured framework for fast, easy and documented API development with Flask
  -
    name: Flask-FS
    url: https://github.com/noirbizarre/flask-fs
    description: Simple and easy file storages for Flask
  -
    name: Gonja
    url: https://github.com/noirbizarre/gonja
    description: A Jinja2 implementation in Go

---

Some of my open-source projects
